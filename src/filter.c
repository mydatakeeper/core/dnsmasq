/* dnsmasq is Copyright (c) 2000-2018 Simon Kelley

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 dated June, 1991, or
   (at your option) version 3 dated 29 June, 2007.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dnsmasq.h"

#ifdef HAVE_DBUS

#include <dbus/dbus.h>

int filter_domain_name(const char * name)
{
  DBusMessage* msg = dbus_message_new_method_call(
    daemon->filter_name,
    daemon->filter_path,
    daemon->filter_interface,
    daemon->filter_method);
  if (NULL == msg)
    {
      my_syslog(LOG_ERR, _("Cannot create DBus message"));
      return 0;
    }

  DBusMessageIter args;
  dbus_message_iter_init_append(msg, &args);
  if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &name))
    {
      my_syslog(LOG_ERR, _("Cannot append parameter to DBus message"));
      return 0;
    }

  DBusError err;
  DBusMessage* reply = dbus_connection_send_with_reply_and_block(
    daemon->dbus,
    msg,
    -1,
    &err);
  if (NULL == reply)
    {
      my_syslog(LOG_ERR, _("%s: %s"), err.name, err.message);
      return 0;
    }

  if (!dbus_message_iter_init(reply, &args))
    {
      my_syslog(LOG_ERR, _("Malformed DBus message"));
      return 0;
    }

  if (DBUS_TYPE_BOOLEAN != dbus_message_iter_get_arg_type(&args)) 
    {
      my_syslog(LOG_ERR, _("Unexpected DBus reply type"));
      return 0;
    }

  int result;
  dbus_message_iter_get_basic(&args, &result);

  dbus_connection_flush(daemon->dbus);

  dbus_message_unref(msg);
  dbus_message_unref(reply);

  return result;
}

#endif
