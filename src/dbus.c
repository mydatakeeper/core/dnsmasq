/* dnsmasq is Copyright (c) 2000-2018 Simon Kelley

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; version 2 dated June, 1991, or
   (at your option) version 3 dated 29 June, 2007.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
     
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dnsmasq.h"

#ifdef HAVE_DBUS

#include <dbus/dbus.h>

const char* introspection_xml_template =
"<!DOCTYPE node PUBLIC \"-//freedesktop//DTD D-BUS Object Introspection 1.0//EN\"\n"
"\"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">\n"
"<node name=\"" DNSMASQ_PATH "\">\n"
"  <interface name=\"org.freedesktop.DBus.Introspectable\">\n"
"    <method name=\"Introspect\">\n"
"      <arg name=\"data\" direction=\"out\" type=\"s\"/>\n"
"    </method>\n"
"  </interface>\n"
"  <interface name=\"%s\">\n"
"    <method name=\"ClearCache\">\n"
"    </method>\n"
"    <method name=\"GetVersion\">\n"
"      <arg name=\"version\" direction=\"out\" type=\"s\"/>\n"
"    </method>\n"
#ifdef HAVE_LOOP
"    <method name=\"GetLoopServers\">\n"
"      <arg name=\"server\" direction=\"out\" type=\"as\"/>\n"
"    </method>\n"
#endif
"    <method name=\"SetServers\">\n"
"      <arg name=\"servers\" direction=\"in\" type=\"av\"/>\n"
"    </method>\n"
"    <method name=\"SetDomainServers\">\n"
"      <arg name=\"servers\" direction=\"in\" type=\"as\"/>\n"
"    </method>\n"
"    <method name=\"SetServersEx\">\n"
"      <arg name=\"servers\" direction=\"in\" type=\"aas\"/>\n"
"    </method>\n"
"    <method name=\"SetFilterWin2KOption\">\n"
"      <arg name=\"filterwin2k\" direction=\"in\" type=\"b\"/>\n"
"    </method>\n"
"    <method name=\"SetBogusPrivOption\">\n"
"      <arg name=\"boguspriv\" direction=\"in\" type=\"b\"/>\n"
"    </method>\n"
"    <signal name=\"DhcpLeaseAdded\">\n"
"      <arg name=\"ipaddr\" type=\"s\"/>\n"
"      <arg name=\"hwaddr\" type=\"s\"/>\n"
"      <arg name=\"metadata\" type=\"a{ss}\"/>\n"
"    </signal>\n"
"    <signal name=\"DhcpLeaseDeleted\">\n"
"      <arg name=\"ipaddr\" type=\"s\"/>\n"
"      <arg name=\"hwaddr\" type=\"s\"/>\n"
"      <arg name=\"metadata\" type=\"a{ss}\"/>\n"
"    </signal>\n"
"    <signal name=\"DhcpLeaseUpdated\">\n"
"      <arg name=\"ipaddr\" type=\"s\"/>\n"
"      <arg name=\"hwaddr\" type=\"s\"/>\n"
"      <arg name=\"metadata\" type=\"a{ss}\"/>\n"
"    </signal>\n"
#ifdef HAVE_DHCP
"    <method name=\"GetDhcpLeases\">\n"
"      <arg name=\"leases\" type=\"a(ssa{ss})\" direction=\"out\"/>\n"
"    </method>\n"
"    <method name=\"AddDhcpLease\">\n"
"       <arg name=\"ipaddr\" type=\"s\"/>\n"
"       <arg name=\"hwaddr\" type=\"s\"/>\n"
"       <arg name=\"hostname\" type=\"ay\"/>\n"
"       <arg name=\"clid\" type=\"ay\"/>\n"
"       <arg name=\"lease_duration\" type=\"u\"/>\n"
"       <arg name=\"ia_id\" type=\"u\"/>\n"
"       <arg name=\"is_temporary\" type=\"b\"/>\n"
"    </method>\n"
"    <method name=\"DeleteDhcpLease\">\n"
"       <arg name=\"ipaddr\" type=\"s\"/>\n"
"       <arg name=\"success\" type=\"b\" direction=\"out\"/>\n"
"    </method>\n"
#endif
"    <signal name=\"ResolveDomain\">\n"
"      <arg name=\"source\" type=\"s\"/>\n"
"      <arg name=\"id\" type=\"q\"/>\n"
"      <arg name=\"query\" type=\"b\"/>\n"
"      <arg name=\"operation_code\" type=\"y\"/>\n"
"      <arg name=\"autoritative_answer\" type=\"b\"/>\n"
"      <arg name=\"truncation\" type=\"b\"/>\n"
"      <arg name=\"recursion_desired\" type=\"b\"/>\n"
"      <arg name=\"recursion_available\" type=\"b\"/>\n"
"      <arg name=\"authenticated_data\" type=\"b\"/>\n"
"      <arg name=\"checking_disabled\" type=\"b\"/>\n"
"      <arg name=\"response_code\" type=\"y\"/>\n"
"      <arg name=\"questions\" type=\"a(sss)\"/>\n"
"      <arg name=\"answers\" type=\"a(sssuv)\"/>\n"
"      <arg name=\"authorities\" type=\"a(sssuv)\"/>\n"
"      <arg name=\"additionals\" type=\"a(sssuv)\"/>\n"
"      <arg name=\"pseudoheaders\" type=\"a(qqqa(qay))\"/>\n"
"    </signal>\n"
"    <method name=\"GetMetrics\">\n"
"      <arg name=\"metrics\" direction=\"out\" type=\"a{su}\"/>\n"
"    </method>\n"
"  </interface>\n"
"</node>\n";

static char *introspection_xml = NULL;

struct watch {
  DBusWatch *watch;      
  struct watch *next;
};


static dbus_bool_t add_watch(DBusWatch *watch, void *data)
{
  struct watch *w;

  for (w = daemon->watches; w; w = w->next)
    if (w->watch == watch)
      return TRUE;

  if (!(w = whine_malloc(sizeof(struct watch))))
    return FALSE;

  w->watch = watch;
  w->next = daemon->watches;
  daemon->watches = w;

  w = data; /* no warning */
  return TRUE;
}

static void remove_watch(DBusWatch *watch, void *data)
{
  struct watch **up, *w, *tmp;  
  
  for (up = &(daemon->watches), w = daemon->watches; w; w = tmp)
    {
      tmp = w->next;
      if (w->watch == watch)
	{
	  *up = tmp;
	  free(w);
	}
      else
	up = &(w->next);
    }

  w = data; /* no warning */
}

static void dbus_read_servers(DBusMessage *message)
{
  DBusMessageIter iter;
  union  mysockaddr addr, source_addr;
  char *domain;
  
  dbus_message_iter_init(message, &iter);

  mark_servers(SERV_FROM_DBUS);
  
  while (1)
    {
      int skip = 0;

      if (dbus_message_iter_get_arg_type(&iter) == DBUS_TYPE_UINT32)
	{
	  u32 a;
	  
	  dbus_message_iter_get_basic(&iter, &a);
	  dbus_message_iter_next (&iter);
	  
#ifdef HAVE_SOCKADDR_SA_LEN
	  source_addr.in.sin_len = addr.in.sin_len = sizeof(struct sockaddr_in);
#endif
	  addr.in.sin_addr.s_addr = ntohl(a);
	  source_addr.in.sin_family = addr.in.sin_family = AF_INET;
	  addr.in.sin_port = htons(NAMESERVER_PORT);
	  source_addr.in.sin_addr.s_addr = INADDR_ANY;
	  source_addr.in.sin_port = htons(daemon->query_port);
	}
      else if (dbus_message_iter_get_arg_type(&iter) == DBUS_TYPE_BYTE)
	{
	  unsigned char p[sizeof(struct in6_addr)];
	  unsigned int i;

	  skip = 1;

	  for(i = 0; i < sizeof(struct in6_addr); i++)
	    {
	      dbus_message_iter_get_basic(&iter, &p[i]);
	      dbus_message_iter_next (&iter);
	      if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_BYTE)
		{
		  i++;
		  break;
		}
	    }

#ifndef HAVE_IPV6
	  my_syslog(LOG_WARNING, _("attempt to set an IPv6 server address via DBus - no IPv6 support"));
#else
	  if (i == sizeof(struct in6_addr))
	    {
	      memcpy(&addr.in6.sin6_addr, p, sizeof(struct in6_addr));
#ifdef HAVE_SOCKADDR_SA_LEN
              source_addr.in6.sin6_len = addr.in6.sin6_len = sizeof(struct sockaddr_in6);
#endif
              source_addr.in6.sin6_family = addr.in6.sin6_family = AF_INET6;
              addr.in6.sin6_port = htons(NAMESERVER_PORT);
              source_addr.in6.sin6_flowinfo = addr.in6.sin6_flowinfo = 0;
	      source_addr.in6.sin6_scope_id = addr.in6.sin6_scope_id = 0;
              source_addr.in6.sin6_addr = in6addr_any;
              source_addr.in6.sin6_port = htons(daemon->query_port);
	      skip = 0;
	    }
#endif
	}
      else
	/* At the end */
	break;
      
      /* process each domain */
      do {
	if (dbus_message_iter_get_arg_type(&iter) == DBUS_TYPE_STRING)
	  {
	    dbus_message_iter_get_basic(&iter, &domain);
	    dbus_message_iter_next (&iter);
	  }
	else
	  domain = NULL;
	
	if (!skip)
	  add_update_server(SERV_FROM_DBUS, &addr, &source_addr, NULL, domain);
     
      } while (dbus_message_iter_get_arg_type(&iter) == DBUS_TYPE_STRING); 
    }
   
  /* unlink and free anything still marked. */
  cleanup_servers();
}

#ifdef HAVE_LOOP
static DBusMessage *dbus_reply_server_loop(DBusMessage *message)
{
  DBusMessageIter args, args_iter;
  struct server *serv;
  DBusMessage *reply = dbus_message_new_method_return(message);
   
  dbus_message_iter_init_append (reply, &args);
  dbus_message_iter_open_container (&args, DBUS_TYPE_ARRAY,DBUS_TYPE_STRING_AS_STRING, &args_iter);

  for (serv = daemon->servers; serv; serv = serv->next)
    if (serv->flags & SERV_LOOP)
      {
	prettyprint_addr(&serv->addr, daemon->addrbuff);
	dbus_message_iter_append_basic (&args_iter, DBUS_TYPE_STRING, &daemon->addrbuff);
      }
  
  dbus_message_iter_close_container (&args, &args_iter);

  return reply;
}
#endif

static DBusMessage* dbus_read_servers_ex(DBusMessage *message, int strings)
{
  DBusMessageIter iter, array_iter, string_iter;
  DBusMessage *error = NULL;
  const char *addr_err;
  char *dup = NULL;
  
  if (!dbus_message_iter_init(message, &iter))
    {
      return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
                                    "Failed to initialize dbus message iter");
    }

  /* check that the message contains an array of arrays */
  if ((dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_ARRAY) ||
      (dbus_message_iter_get_element_type(&iter) != (strings ? DBUS_TYPE_STRING : DBUS_TYPE_ARRAY)))
    {
      return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
                                    strings ? "Expected array of string" : "Expected array of string arrays");
     }
 
  mark_servers(SERV_FROM_DBUS);

  /* array_iter points to each "as" element in the outer array */
  dbus_message_iter_recurse(&iter, &array_iter);
  while (dbus_message_iter_get_arg_type(&array_iter) != DBUS_TYPE_INVALID)
    {
      const char *str = NULL;
      union  mysockaddr addr, source_addr;
      int flags = 0;
      char interface[IF_NAMESIZE];
      char *str_addr, *str_domain = NULL;

      if (strings)
	{
	  dbus_message_iter_get_basic(&array_iter, &str);
	  if (!str || !strlen (str))
	    {
	      error = dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
					     "Empty string");
	      break;
	    }
	  
	  /* dup the string because it gets modified during parsing */
	  if (dup)
	    free(dup);
	  if (!(dup = str_domain = whine_malloc(strlen(str)+1)))
	    break;
	  
	  strcpy(str_domain, str);

	  /* point to address part of old string for error message */
	  if ((str_addr = strrchr(str, '/')))
	    str = str_addr+1;
	  
	  if ((str_addr = strrchr(str_domain, '/')))
	    {
	      if (*str_domain != '/' || str_addr == str_domain)
		{
		  error = dbus_message_new_error_printf(message,
							DBUS_ERROR_INVALID_ARGS,
							"No domain terminator '%s'",
							str);
		  break;
		}
	      *str_addr++ = 0;
	      str_domain++;
	    }
	  else
	    {
	      str_addr = str_domain;
	      str_domain = NULL;
	    }

	  
	}
      else
	{
	  /* check the types of the struct and its elements */
	  if ((dbus_message_iter_get_arg_type(&array_iter) != DBUS_TYPE_ARRAY) ||
	      (dbus_message_iter_get_element_type(&array_iter) != DBUS_TYPE_STRING))
	    {
	      error = dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
					     "Expected inner array of strings");
	      break;
	    }
	  
	  /* string_iter points to each "s" element in the inner array */
	  dbus_message_iter_recurse(&array_iter, &string_iter);
	  if (dbus_message_iter_get_arg_type(&string_iter) != DBUS_TYPE_STRING)
	    {
	      /* no IP address given */
	      error = dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
					     "Expected IP address");
	      break;
	    }
	  
	  dbus_message_iter_get_basic(&string_iter, &str);
	  if (!str || !strlen (str))
	    {
	      error = dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
					     "Empty IP address");
	      break;
	    }
	  
	  /* dup the string because it gets modified during parsing */
	  if (dup)
	    free(dup);
	  if (!(dup = str_addr = whine_malloc(strlen(str)+1)))
	    break;
	  
	  strcpy(str_addr, str);
	}

      memset(&addr, 0, sizeof(addr));
      memset(&source_addr, 0, sizeof(source_addr));
      memset(&interface, 0, sizeof(interface));

      /* parse the IP address */
      if ((addr_err = parse_server(str_addr, &addr, &source_addr, (char *) &interface, &flags)))
	{
          error = dbus_message_new_error_printf(message, DBUS_ERROR_INVALID_ARGS,
                                                "Invalid IP address '%s': %s",
                                                str, addr_err);
          break;
        }
      
      /* 0.0.0.0 for server address == NULL, for Dbus */
      if (addr.in.sin_family == AF_INET &&
          addr.in.sin_addr.s_addr == 0)
        flags |= SERV_NO_ADDR;
      
      if (strings)
	{
	  char *p;
	  
	  do {
	    if (str_domain)
	      {
		if ((p = strchr(str_domain, '/')))
		  *p++ = 0;
	      }
	    else 
	      p = NULL;
	    
	    add_update_server(flags | SERV_FROM_DBUS, &addr, &source_addr, interface, str_domain);
	  } while ((str_domain = p));
	}
      else
	{
	  /* jump past the address to the domain list (if any) */
	  dbus_message_iter_next (&string_iter);
	  
	  /* parse domains and add each server/domain pair to the list */
	  do {
	    str = NULL;
	    if (dbus_message_iter_get_arg_type(&string_iter) == DBUS_TYPE_STRING)
	      dbus_message_iter_get_basic(&string_iter, &str);
	    dbus_message_iter_next (&string_iter);
	    
	    add_update_server(flags | SERV_FROM_DBUS, &addr, &source_addr, interface, str);
	  } while (dbus_message_iter_get_arg_type(&string_iter) == DBUS_TYPE_STRING);
	}
	 
      /* jump to next element in outer array */
      dbus_message_iter_next(&array_iter);
    }

  cleanup_servers();

  if (dup)
    free(dup);

  return error;
}

static DBusMessage *dbus_set_bool(DBusMessage *message, int flag, char *name)
{
  DBusMessageIter iter;
  dbus_bool_t enabled;

  if (!dbus_message_iter_init(message, &iter) || dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_BOOLEAN)
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS, "Expected boolean argument");
  
  dbus_message_iter_get_basic(&iter, &enabled);

  if (enabled)
    { 
      my_syslog(LOG_INFO, _("Enabling --%s option from D-Bus"), name);
      set_option_bool(flag);
    }
  else
    {
      my_syslog(LOG_INFO, _("Disabling --%s option from D-Bus"), name);
      reset_option_bool(flag);
    }

  return NULL;
}

#ifdef HAVE_DHCP
static inline void
add_extradata(DBusMessageIter* dict, char *key, char *val)
{
  DBusMessageIter entry;
  dbus_message_iter_open_container(dict, DBUS_TYPE_DICT_ENTRY, NULL, &entry);
  dbus_message_iter_append_basic(&entry, DBUS_TYPE_STRING, &key);
  dbus_message_iter_append_basic(&entry, DBUS_TYPE_STRING, &val);
  dbus_message_iter_close_container(dict, &entry);
}

static inline unsigned char *
add_buffer_extradata(unsigned char *buf, unsigned char *end, char *key, DBusMessageIter* dict)
{
  unsigned char *next = NULL;
  char *val = NULL;

  if (buf && (buf != end))
    {
      for (next = buf; ; next++)
        if (next == end)
          {
            next = NULL;
            break;
          }
        else if (*next == 0)
          break;

      if (next && (next != buf))
        {
          char *p;
          /* No "=" in value */
          if ((p = strchr((char *)buf, '=')))
            *p = 0;
          val = (char *)buf;
        }
    }

  if (val != NULL)
      add_extradata(dict, key, val);

  return next ? next + 1 : NULL;
}

static dbus_bool_t
add_lease_extradata(DBusMessageIter *msg, struct dhcp_lease *lease, char* hostname)
{
  DBusMessageIter extra;
  unsigned char *buf, *end;
  int i;

  dbus_message_iter_open_container(msg, DBUS_TYPE_ARRAY, "{ss}", &extra);

#ifdef HAVE_BROKEN_RTC
  sprintf(daemon->dhcp_buff2, "%u", lease->length);
  add_extradata(&extra, "DNSMASQ_LEASE_LENGTH", daemon->dhcp_buff2);
#else
  sprintf(daemon->dhcp_buff2, "%lu", (unsigned long)lease->expires);
  add_extradata(&extra, "DNSMASQ_LEASE_EXPIRES", daemon->dhcp_buff2);
#endif

  add_extradata(&extra, "DNSMASQ_DOMAIN", hostname);

  end = lease->extradata + lease->extradata_len;
  buf = lease->extradata;

  if (!(lease->flags & F_IPV6))
  buf = add_buffer_extradata(buf, end, "DNSMASQ_VENDOR_CLASS", &extra);
#ifdef HAVE_DHCP6
  else
    {
      if (lease->vendorclass_count != 0)
        {
          buf = add_buffer_extradata(buf, end, "DNSMASQ_VENDOR_CLASS_ID", &extra);
          for (i = 0; i < lease->vendorclass_count - 1; i++)
            {
              sprintf(daemon->dhcp_buff2, "DNSMASQ_VENDOR_CLASS%i", i);
              buf = add_buffer_extradata(buf, end, daemon->dhcp_buff2, &extra);
            }
        }
    }
#endif

  buf = add_buffer_extradata(buf, end, "DNSMASQ_SUPPLIED_HOSTNAME", &extra);

  if (!(lease->flags & F_IPV6))
    {
      buf = add_buffer_extradata(buf, end, "DNSMASQ_CPEWAN_OUI", &extra);
      buf = add_buffer_extradata(buf, end, "DNSMASQ_CPEWAN_SERIAL", &extra);
      buf = add_buffer_extradata(buf, end, "DNSMASQ_CPEWAN_CLASS", &extra);
      buf = add_buffer_extradata(buf, end, "DNSMASQ_CIRCUIT_ID", &extra);
      buf = add_buffer_extradata(buf, end, "DNSMASQ_SUBSCRIBER_ID", &extra);
      buf = add_buffer_extradata(buf, end, "DNSMASQ_REMOTE_ID", &extra);
      buf = add_buffer_extradata(buf, end, "DNSMASQ_REQUESTED_OPTIONS", &extra);
    }

  buf = add_buffer_extradata(buf, end, "DNSMASQ_TAGS", &extra);

  if (lease->flags & F_IPV6)
    buf = add_buffer_extradata(buf, end, "DNSMASQ_RELAY_ADDRESS", &extra);

  for (i = 0; buf; i++)
    {
      sprintf(daemon->dhcp_buff2, "DNSMASQ_USER_CLASS%i", i);
      buf = add_buffer_extradata(buf, end, daemon->dhcp_buff2, &extra);
    }

  dbus_message_iter_close_container(msg, &extra);

  return TRUE;
}

static DBusMessage *dbus_get_leases(DBusMessage* message)
{
  struct dhcp_lease *lease;
  DBusMessage *reply = dbus_message_new_method_return(message);
  DBusMessageIter array, strct, iter;
  char *hostname, *mac = daemon->namebuff;
  unsigned char *p;
  int i;

  dbus_message_iter_init_append(reply, &iter);
  dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "(ssa{ss})", &array);

  for (lease = daemon->leases; lease; lease = lease->next)
    {
      hostname = lease->hostname;
      if (!hostname)
        hostname = "";

#ifdef HAVE_DHCP6
      if (lease->flags & (LEASE_TA | LEASE_NA))
        {
          print_mac(mac, lease->clid, lease->clid_len);
          inet_ntop(AF_INET6, &lease->addr6, daemon->addrbuff, ADDRSTRLEN);
        }
      else
#endif
        {
          p = extended_hwaddr(lease->hwaddr_type, lease->hwaddr_len,
              lease->hwaddr, lease->clid_len, lease->clid, &i);
          print_mac(mac, p, i);
          inet_ntop(AF_INET, &lease->addr, daemon->addrbuff, ADDRSTRLEN);
        }

      dbus_message_iter_open_container(&array, DBUS_TYPE_STRUCT, NULL, &strct);
      dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &daemon->addrbuff);
      dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &mac);

      add_lease_extradata(&strct, lease, hostname);

      dbus_message_iter_close_container(&array, &strct);
    }

  dbus_message_iter_close_container(&iter, &array);

  return reply;
}

static DBusMessage *dbus_add_lease(DBusMessage* message)
{
  struct dhcp_lease *lease;
  const char *ipaddr, *hwaddr, *hostname, *tmp;
  const unsigned char* clid;
  int clid_len, hostname_len, hw_len, hw_type;
  dbus_uint32_t expires, ia_id;
  dbus_bool_t is_temporary;
  struct all_addr addr;
  time_t now = dnsmasq_time();
  unsigned char dhcp_chaddr[DHCP_CHADDR_MAX];

  DBusMessageIter iter, array_iter;
  if (!dbus_message_iter_init(message, &iter))
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Failed to initialize dbus message iter");

  if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_STRING)
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Expected string as first argument");

  dbus_message_iter_get_basic(&iter, &ipaddr);
  dbus_message_iter_next(&iter);

  if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_STRING)
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Expected string as second argument");
    
  dbus_message_iter_get_basic(&iter, &hwaddr);
  dbus_message_iter_next(&iter);

  if ((dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_ARRAY) ||
      (dbus_message_iter_get_element_type(&iter) != DBUS_TYPE_BYTE))
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Expected byte array as third argument");
    
  dbus_message_iter_recurse(&iter, &array_iter);
  dbus_message_iter_get_fixed_array(&array_iter, &hostname, &hostname_len);
  tmp = memchr(hostname, '\0', hostname_len);
  if (tmp)
    {
      if (tmp == &hostname[hostname_len - 1])
	hostname_len--;
      else
	return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				      "Hostname contains an embedded NUL character");
    }
  dbus_message_iter_next(&iter);

  if ((dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_ARRAY) ||
      (dbus_message_iter_get_element_type(&iter) != DBUS_TYPE_BYTE))
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Expected byte array as fourth argument");

  dbus_message_iter_recurse(&iter, &array_iter);
  dbus_message_iter_get_fixed_array(&array_iter, &clid, &clid_len);
  dbus_message_iter_next(&iter);

  if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_UINT32)
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Expected uint32 as fifth argument");
    
  dbus_message_iter_get_basic(&iter, &expires);
  dbus_message_iter_next(&iter);

  if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_UINT32)
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
                                    "Expected uint32 as sixth argument");
  
  dbus_message_iter_get_basic(&iter, &ia_id);
  dbus_message_iter_next(&iter);

  if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_BOOLEAN)
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Expected uint32 as sixth argument");

  dbus_message_iter_get_basic(&iter, &is_temporary);

  if (inet_pton(AF_INET, ipaddr, &addr.addr.addr4))
    {
      if (ia_id != 0 || is_temporary)
	return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				      "ia_id and is_temporary must be zero for IPv4 lease");
      
      if (!(lease = lease_find_by_addr(addr.addr.addr4)))
    	lease = lease4_allocate(addr.addr.addr4);
    }
#ifdef HAVE_DHCP6
  else if (inet_pton(AF_INET6, ipaddr, &addr.addr.addr6))
    {
      if (!(lease = lease6_find_by_addr(&addr.addr.addr6, 128, 0)))
	lease = lease6_allocate(&addr.addr.addr6,
				is_temporary ? LEASE_TA : LEASE_NA);
      lease_set_iaid(lease, ia_id);
    }
#endif
  else
    return dbus_message_new_error_printf(message, DBUS_ERROR_INVALID_ARGS,
					 "Invalid IP address '%s'", ipaddr);
   
  hw_len = parse_hex((char*)hwaddr, dhcp_chaddr, DHCP_CHADDR_MAX, NULL, &hw_type);
  if (hw_type == 0 && hw_len != 0)
    hw_type = ARPHRD_ETHER;
  
  lease_set_hwaddr(lease, dhcp_chaddr, clid, hw_len, hw_type,
                   clid_len, now, 0);
  lease_set_expires(lease, expires, now);
  if (hostname_len != 0)
    lease_set_hostname(lease, hostname, 0, get_domain(lease->addr), NULL);
  
  lease_update_file(now);
  lease_update_dns(0);

  return NULL;
}

static DBusMessage *dbus_del_lease(DBusMessage* message)
{
  struct dhcp_lease *lease;
  DBusMessageIter iter;
  const char *ipaddr;
  DBusMessage *reply;
  struct all_addr addr;
  dbus_bool_t ret = 1;
  time_t now = dnsmasq_time();

  if (!dbus_message_iter_init(message, &iter))
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Failed to initialize dbus message iter");
   
  if (dbus_message_iter_get_arg_type(&iter) != DBUS_TYPE_STRING)
    return dbus_message_new_error(message, DBUS_ERROR_INVALID_ARGS,
				  "Expected string as first argument");
   
  dbus_message_iter_get_basic(&iter, &ipaddr);

  if (inet_pton(AF_INET, ipaddr, &addr.addr.addr4))
    lease = lease_find_by_addr(addr.addr.addr4);
#ifdef HAVE_DHCP6
  else if (inet_pton(AF_INET6, ipaddr, &addr.addr.addr6))
    lease = lease6_find_by_addr(&addr.addr.addr6, 128, 0);
#endif
  else
    return dbus_message_new_error_printf(message, DBUS_ERROR_INVALID_ARGS,
					 "Invalid IP address '%s'", ipaddr);
    
  if (lease)
    {
      lease_prune(lease, now);
      lease_update_file(now);
      lease_update_dns(0);
    }
  else
    ret = 0;
  
  if ((reply = dbus_message_new_method_return(message)))
    dbus_message_append_args(reply, DBUS_TYPE_BOOLEAN, &ret,
			     DBUS_TYPE_INVALID);
  
    
  return reply;
}
#endif

static DBusMessage *dbus_get_metrics(DBusMessage* message)
{
  DBusMessage *reply = dbus_message_new_method_return(message);
  DBusMessageIter array, dict, iter;
  int i;

  dbus_message_iter_init_append(reply, &iter);
  dbus_message_iter_open_container(&iter, DBUS_TYPE_ARRAY, "{su}", &array);

  for (i = 0; i < __METRIC_MAX; i++) {
    const char *key     = get_metric_name(i);
    dbus_uint32_t value = daemon->metrics[i];

    dbus_message_iter_open_container(&array, DBUS_TYPE_DICT_ENTRY, NULL, &dict);
    dbus_message_iter_append_basic(&dict, DBUS_TYPE_STRING, &key);
    dbus_message_iter_append_basic(&dict, DBUS_TYPE_UINT32, &value);
    dbus_message_iter_close_container(&array, &dict);
  }

  dbus_message_iter_close_container(&iter, &array);

  return reply;
}

DBusHandlerResult message_handler(DBusConnection *connection, 
				  DBusMessage *message, 
				  void *user_data)
{
  char *method = (char *)dbus_message_get_member(message);
  DBusMessage *reply = NULL;
  int clear_cache = 0, new_servers = 0;
    
  if (dbus_message_is_method_call(message, DBUS_INTERFACE_INTROSPECTABLE, "Introspect"))
    {
      /* string length: "%s" provides space for termination zero */
      if (!introspection_xml && 
	  (introspection_xml = whine_malloc(strlen(introspection_xml_template) + strlen(daemon->dbus_name))))
	sprintf(introspection_xml, introspection_xml_template, daemon->dbus_name);
    
      if (introspection_xml)
	{
	  reply = dbus_message_new_method_return(message);
	  dbus_message_append_args(reply, DBUS_TYPE_STRING, &introspection_xml, DBUS_TYPE_INVALID);
	}
    }
  else if (strcmp(method, "GetVersion") == 0)
    {
      char *v = VERSION;
      reply = dbus_message_new_method_return(message);
      
      dbus_message_append_args(reply, DBUS_TYPE_STRING, &v, DBUS_TYPE_INVALID);
    }
#ifdef HAVE_LOOP
  else if (strcmp(method, "GetLoopServers") == 0)
    {
      reply = dbus_reply_server_loop(message);
    }
#endif
  else if (strcmp(method, "SetServers") == 0)
    {
      dbus_read_servers(message);
      new_servers = 1;
    }
  else if (strcmp(method, "SetServersEx") == 0)
    {
      reply = dbus_read_servers_ex(message, 0);
      new_servers = 1;
    }
  else if (strcmp(method, "SetDomainServers") == 0)
    {
      reply = dbus_read_servers_ex(message, 1);
      new_servers = 1;
    }
  else if (strcmp(method, "SetFilterWin2KOption") == 0)
    {
      reply = dbus_set_bool(message, OPT_FILTER, "filterwin2k");
    }
  else if (strcmp(method, "SetBogusPrivOption") == 0)
    {
      reply = dbus_set_bool(message, OPT_BOGUSPRIV, "bogus-priv");
    }
#ifdef HAVE_DHCP
  else if (strcmp(method, "GetDhcpLeases") == 0)
    {
      reply = dbus_get_leases(message);
    }
  else if (strcmp(method, "AddDhcpLease") == 0)
    {
      reply = dbus_add_lease(message);
    }
  else if (strcmp(method, "DeleteDhcpLease") == 0)
    {
      reply = dbus_del_lease(message);
    }
#endif
  else if (strcmp(method, "GetMetrics") == 0)
    {
      reply = dbus_get_metrics(message);
    }
  else if (strcmp(method, "ClearCache") == 0)
    clear_cache = 1;
  else
    return (DBUS_HANDLER_RESULT_NOT_YET_HANDLED);
   
  if (new_servers)
    {
      my_syslog(LOG_INFO, _("setting upstream servers from DBus"));
      check_servers();
      if (option_bool(OPT_RELOAD))
	clear_cache = 1;
    }

  if (clear_cache)
    clear_cache_and_reload(dnsmasq_time());
  
  method = user_data; /* no warning */

  /* If no reply or no error, return nothing */
  if (!reply)
    reply = dbus_message_new_method_return(message);

  if (reply)
    {
      dbus_connection_send (connection, reply, NULL);
      dbus_message_unref (reply);
    }

  return (DBUS_HANDLER_RESULT_HANDLED);
}
 

/* returns NULL or error message, may fail silently if dbus daemon not yet up. */
char *dbus_init(void)
{
  DBusConnection *connection = NULL;
  DBusObjectPathVTable dnsmasq_vtable = {NULL, &message_handler, NULL, NULL, NULL, NULL };
  DBusError dbus_error;
  DBusMessage *message;

  dbus_error_init (&dbus_error);
  if (!(connection = dbus_bus_get (DBUS_BUS_SYSTEM, &dbus_error)))
    return NULL;
    
  dbus_connection_set_exit_on_disconnect(connection, FALSE);
  dbus_connection_set_watch_functions(connection, add_watch, remove_watch, 
				      NULL, NULL, NULL);
  dbus_error_init (&dbus_error);
  dbus_bus_request_name (connection, daemon->dbus_name, 0, &dbus_error);
  if (dbus_error_is_set (&dbus_error))
    return (char *)dbus_error.message;
  
  if (!dbus_connection_register_object_path(connection,  DNSMASQ_PATH, 
					    &dnsmasq_vtable, NULL))
    return _("could not register a DBus message handler");
  
  daemon->dbus = connection; 
  
  if ((message = dbus_message_new_signal(DNSMASQ_PATH, daemon->dbus_name, "Up")))
    {
      dbus_connection_send(connection, message, NULL);
      dbus_message_unref(message);
    }

  return NULL;
}
 

void set_dbus_listeners(void)
{
  struct watch *w;
  
  for (w = daemon->watches; w; w = w->next)
    if (dbus_watch_get_enabled(w->watch))
      {
	unsigned int flags = dbus_watch_get_flags(w->watch);
	int fd = dbus_watch_get_unix_fd(w->watch);
	
	if (flags & DBUS_WATCH_READABLE)
	  poll_listen(fd, POLLIN);
	
	if (flags & DBUS_WATCH_WRITABLE)
	  poll_listen(fd, POLLOUT);
	
	poll_listen(fd, POLLERR);
      }
}

void check_dbus_listeners()
{
  DBusConnection *connection = (DBusConnection *)daemon->dbus;
  struct watch *w;

  for (w = daemon->watches; w; w = w->next)
    if (dbus_watch_get_enabled(w->watch))
      {
	unsigned int flags = 0;
	int fd = dbus_watch_get_unix_fd(w->watch);
	
	if (poll_check(fd, POLLIN))
	  flags |= DBUS_WATCH_READABLE;
	
	if (poll_check(fd, POLLOUT))
	  flags |= DBUS_WATCH_WRITABLE;
	
	if (poll_check(fd, POLLERR))
	  flags |= DBUS_WATCH_ERROR;

	if (flags != 0)
	  dbus_watch_handle(w->watch, flags);
      }

  if (connection)
    {
      dbus_connection_ref (connection);
      while (dbus_connection_dispatch (connection) == DBUS_DISPATCH_DATA_REMAINS);
      dbus_connection_unref (connection);
    }
}

#ifdef HAVE_DHCP
void emit_dbus_dhcp_lease_signal(int action, struct dhcp_lease *lease, char *hostname)
{
  DBusConnection *connection = (DBusConnection *)daemon->dbus;
  DBusMessage* message = NULL;
  DBusMessageIter args;
  char *action_str, *mac = daemon->namebuff;
  unsigned char *p;
  int i;

  if (!connection)
    return;
  
  if (!hostname)
    hostname = "";
  
#ifdef HAVE_DHCP6
   if (lease->flags & (LEASE_TA | LEASE_NA))
     {
       print_mac(mac, lease->clid, lease->clid_len);
       inet_ntop(AF_INET6, &lease->addr6, daemon->addrbuff, ADDRSTRLEN);
     }
   else
#endif
     {
       p = extended_hwaddr(lease->hwaddr_type, lease->hwaddr_len,
			   lease->hwaddr, lease->clid_len, lease->clid, &i);
       print_mac(mac, p, i);
       inet_ntop(AF_INET, &lease->addr, daemon->addrbuff, ADDRSTRLEN);
     }

  if (action == ACTION_DEL)
    action_str = "DhcpLeaseDeleted";
  else if (action == ACTION_ADD)
    action_str = "DhcpLeaseAdded";
  else if (action == ACTION_OLD)
    action_str = "DhcpLeaseUpdated";
  else
    return;

  if (!(message = dbus_message_new_signal(DNSMASQ_PATH, daemon->dbus_name, action_str)))
    return;
  
  dbus_message_iter_init_append(message, &args);
  
  if (dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &daemon->addrbuff) &&
      dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &mac) &&
      add_lease_extradata(&args, lease, hostname))
    dbus_connection_send(connection, message, NULL);
  
  dbus_message_unref(message);
}
#endif

char *get_class(unsigned char **p)
{
  int class;
  GETSHORT(class, *p);

  switch (class) {
    case C_IN:     return "IN";
    case C_CHAOS:  return "CHAOS";
    case C_HESIOD: return "HESIOD";
    case C_ANY:    return "ANY";
    default:       return NULL;
  }
}

char *get_type(unsigned char **p)
{
  int type;
  GETSHORT(type, *p);

  switch (type) {
    case T_A:      return "A";
    case T_NS:     return "NS";
    case T_MD:     return "MD";
    case T_MF:     return "MF";
    case T_CNAME:  return "CNAME";
    case T_SOA:    return "SOA";
    case T_MB:     return "MB";
    case T_MG:     return "MG";
    case T_MR:     return "MR";
    case T_PTR:    return "PTR";
    case T_HINFO:  return "HINFO";
    case T_MINFO:  return "MINFO";
    case T_MX:     return "MX";
    case T_TXT:    return "TXT";
    case T_RP:     return "RP";
    case T_AFSDB:  return "AFSDB";
    case T_RT:     return "RT";
    case T_SIG:    return "SIG";
    case T_PX:     return "PX";
    case T_AAAA:   return "AAAA";
    case T_NXT:    return "NXT";
    case T_SRV:    return "SRV";
    case T_NAPTR:  return "NAPTR";
    case T_KX:     return "KX";
    case T_DNAME:  return "DNAME";
    case T_OPT:    return "OPT";
    case T_DS:     return "DS";
    case T_RRSIG:  return "RRSIG";
    case T_NSEC:   return "NSEC";
    case T_DNSKEY: return "DNSKEY";
    case T_NSEC3:  return "NSEC3";
    case T_TKEY:   return "TKEY";
    case T_TSIG:   return "TSIG";
    case T_AXFR:   return "AXFR";
    case T_MAILB:  return "MAILB";
    case T_ANY:    return "ANY";
    case T_CAA:    return "CAA";
    default:       return NULL;
  }
}

uint32_t get_ttl(unsigned char **p)
{
  uint32_t ttl;
  GETLONG(ttl, *p);
  return ttl;
}

uint16_t get_rdlength(unsigned char **p)
{
  uint16_t rdlength;
  GETSHORT(rdlength, *p);
  return rdlength;
}

int dbus_message_append_queries(DBusMessageIter *args,
				struct dns_header *header, size_t qlen,
				size_t count, unsigned char **pp)
{
  DBusMessageIter queries;
  if (!dbus_message_iter_open_container(args, DBUS_TYPE_ARRAY, "(sss)", &queries))
    return 0;

  for (size_t i = 0; i < count; i++)
    {
      char qddn[MAXDNAME];
      if (!extract_name(header, qlen, pp, qddn, 1, 0))
	return 0;

      char *type = get_type(pp);
      char *class = get_class(pp);
      if (!type || !class)
	continue;

      char *qddnp = qddn;
      DBusMessageIter strct;
      if (!dbus_message_iter_open_container(&queries, DBUS_TYPE_STRUCT, NULL, &strct) ||
	  !dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &qddnp) ||
	  !dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &type) ||
	  !dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &class) ||
	  !dbus_message_iter_close_container(&queries, &strct))
	return 0;
    }

  if (!dbus_message_iter_close_container(args, &queries))
    return 0;

  return 1;
}

int dbus_message_append_answers(DBusMessageIter *args,
				struct dns_header *header, size_t qlen,
				size_t count, unsigned char **pp)
{
  DBusMessageIter answers;
  if (!dbus_message_iter_open_container(args, DBUS_TYPE_ARRAY, "(sssuv)", &answers))
    return 0;

  for (size_t i = 0; i < count; i++)
    {
      char andn[MAXDNAME];
      if (!extract_name(header, qlen, pp, andn, 1, 0))
	return 0;

      char *type = get_type(pp);
      char *class = get_class(pp);
      uint32_t ttl = get_ttl(pp);
      uint16_t len = get_rdlength(pp);
      if (!type || !class)
	{
	  *pp += len;
	  continue;
	}

      char *andnp = andn;
      DBusMessageIter strct;
      if (!dbus_message_iter_open_container(&answers, DBUS_TYPE_STRUCT, NULL, &strct) ||
	  !dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &andnp) ||
	  !dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &type) ||
	  !dbus_message_iter_append_basic(&strct, DBUS_TYPE_STRING, &class) ||
	  !dbus_message_iter_append_basic(&strct, DBUS_TYPE_UINT32, &ttl))
	return 0;

      if (strcmp(class, "IN"))
	goto unknown;
      else if (!strcmp(type, "A"))
	{
	  char addr[ADDRSTRLEN];
	  if (!inet_ntop(AF_INET, (struct all_addr *)*pp, addr, ADDRSTRLEN))
	    return 0;
	  char *addrp = addr;
	  DBusMessageIter var;
	  if (!dbus_message_iter_open_container(&strct, DBUS_TYPE_VARIANT, "s", &var) ||
	      !dbus_message_iter_append_basic(&var, DBUS_TYPE_STRING, &addrp) ||
	      !dbus_message_iter_close_container(&strct, &var))
	    return 0;
	  *pp+=len;
	}
      else if (!strcmp(type, "AAAA"))
	{
	  char addr[ADDRSTRLEN];
	  if (!inet_ntop(AF_INET6, (struct all_addr *)*pp, addr, ADDRSTRLEN))
	    return 0;
	  char *addrp = addr;
	  DBusMessageIter var;
	  if (!dbus_message_iter_open_container(&strct, DBUS_TYPE_VARIANT, "s", &var) ||
	      !dbus_message_iter_append_basic(&var, DBUS_TYPE_STRING, &addrp) ||
	      !dbus_message_iter_close_container(&strct, &var))
	    return 0;
	  *pp+=len;
	}
      else if (!strcmp(type, "CNAME") || !strcmp(type, "PTR") || !strcmp(type, "NS"))
	{
	  char dn[MAXDNAME];
	  if (!extract_name(header, qlen, pp, dn, 1, 0))
	    return 0;
	  char *dnp = dn;
	  DBusMessageIter var;
	  if (!dbus_message_iter_open_container(&strct, DBUS_TYPE_VARIANT, "s", &var) ||
	      !dbus_message_iter_append_basic(&var, DBUS_TYPE_STRING, &dnp) ||
	      !dbus_message_iter_close_container(&strct, &var))
	    return 0;
	}
      else if (!strcmp(type, "TXT"))
	{
	  unsigned char sz = **pp;
	  *pp += 1;
	  char *txt = strndup((char*) *pp, sz);
	  if (!txt)
	    return 0;
	  DBusMessageIter var;
	  if (!dbus_message_iter_open_container(&strct, DBUS_TYPE_VARIANT, "s", &var) ||
	      !dbus_message_iter_append_basic(&var, DBUS_TYPE_STRING, &txt) ||
	      !dbus_message_iter_close_container(&strct, &var))
	    {
	      free(txt);
	      return 0;
	    }
	  *pp += sz;
	  free(txt);
	}
      else if (!strcmp(type, "MX"))
	{
	  uint16_t priority;
	  GETSHORT(priority, *pp);
	  char dn[MAXDNAME];
	  if (!extract_name(header, qlen, pp, dn, 1, 0))
	    return 0;
	  char *dnp = dn;
	  DBusMessageIter var, mx;
	  if (!dbus_message_iter_open_container(&strct, DBUS_TYPE_VARIANT, "(qs)", &var) ||
	      !dbus_message_iter_open_container(&var, DBUS_TYPE_STRUCT, NULL, &mx) ||
	      !dbus_message_iter_append_basic(&mx, DBUS_TYPE_UINT16, &priority) ||
	      !dbus_message_iter_append_basic(&mx, DBUS_TYPE_STRING, &dnp) ||
	      !dbus_message_iter_close_container(&var, &mx) ||
	      !dbus_message_iter_close_container(&strct, &var))
	    return 0;
	}
      else if (!strcmp(type, "SOA"))
	{
	  char mname[MAXDNAME], rname[MAXDNAME];
	  if (!extract_name(header, qlen, pp, mname, 1, 0) ||
	      !extract_name(header, qlen, pp, rname, 1, 0))
	    return 0;
	  uint16_t serial, refresh, retry, expire, minimum;
	  GETLONG(serial, *pp);
	  GETLONG(refresh, *pp);
	  GETLONG(retry, *pp);
	  GETLONG(expire, *pp);
	  GETLONG(minimum, *pp);
	  char *mnamep = mname, *rnamep = rname;
	  DBusMessageIter var, soa;
	  if (!dbus_message_iter_open_container(&strct, DBUS_TYPE_VARIANT, "(ssuuuuu)", &var) ||
	      !dbus_message_iter_open_container(&var, DBUS_TYPE_STRUCT, NULL, &soa) ||
	      !dbus_message_iter_append_basic(&soa, DBUS_TYPE_STRING, &mnamep) ||
	      !dbus_message_iter_append_basic(&soa, DBUS_TYPE_STRING, &rnamep) ||
	      !dbus_message_iter_append_basic(&soa, DBUS_TYPE_UINT32, &serial) ||
	      !dbus_message_iter_append_basic(&soa, DBUS_TYPE_UINT32, &refresh) ||
	      !dbus_message_iter_append_basic(&soa, DBUS_TYPE_UINT32, &retry) ||
	      !dbus_message_iter_append_basic(&soa, DBUS_TYPE_UINT32, &expire) ||
	      !dbus_message_iter_append_basic(&soa, DBUS_TYPE_UINT32, &minimum) ||
	      !dbus_message_iter_close_container(&var, &soa) ||
	      !dbus_message_iter_close_container(&strct, &var))
	    return 0;
	}
      else
unknown:
	{
	  DBusMessageIter var, arr;
	  if (!dbus_message_iter_open_container(&strct, DBUS_TYPE_VARIANT, "ay", &var) ||
	      !dbus_message_iter_open_container(&var, DBUS_TYPE_ARRAY, "y", &arr))
	    return 0;
	  for (size_t j = 0; j < len; ++j, ++(*pp))
	    if (!dbus_message_iter_append_basic(&arr, DBUS_TYPE_BYTE, *pp))
	      return 0;
	  if (!dbus_message_iter_close_container(&var, &arr) ||
	      !dbus_message_iter_close_container(&strct, &var))
	    return 0;
	}
      if (!dbus_message_iter_close_container(&answers, &strct))
	return 0;
    }

  if (!dbus_message_iter_close_container(args, &answers))
   return 0;

  return 1;
}

int dbus_message_append_pseudoheaders(DBusMessageIter *args,
				      struct dns_header *header, size_t qlen,
				      size_t count, unsigned char **pp)
{
  DBusMessageIter pseudoheaders;
  if (!dbus_message_iter_open_container(args, DBUS_TYPE_ARRAY, "(qqqa(qay))", &pseudoheaders))
    return 0;

  for (size_t i = 0; i < count; i++)
    {
      *pp = skip_name(*pp, header, qlen, 1);

      char *type = get_type(pp);
      uint16_t udp_sz, rcode, flags, rdlen;
      GETSHORT(udp_sz, *pp);
      GETSHORT(rcode, *pp);
      GETSHORT(flags, *pp);
      GETSHORT(rdlen, *pp);

      if (strcmp(type, "OPT"))
	{
	  *pp += rdlen;
	  continue;
	}

      DBusMessageIter pseudoheader, options;
      if (!dbus_message_iter_open_container(&pseudoheaders, DBUS_TYPE_STRUCT, NULL, &pseudoheader) ||
          !dbus_message_iter_append_basic(&pseudoheader, DBUS_TYPE_UINT16, &udp_sz) ||
	  !dbus_message_iter_append_basic(&pseudoheader, DBUS_TYPE_UINT16, &rcode) ||
	  !dbus_message_iter_append_basic(&pseudoheader, DBUS_TYPE_UINT16, &flags) ||
	  !dbus_message_iter_open_container(&pseudoheader, DBUS_TYPE_ARRAY, "(qay)", &options))
	return 0;

      while (rdlen > 0)
	{
	  uint16_t code, len;
	  GETSHORT(code, *pp);
	  GETSHORT(len, *pp);

	  DBusMessageIter strct, arr;
	  if (!dbus_message_iter_open_container(&options, DBUS_TYPE_STRUCT, NULL, &strct) ||
	      !dbus_message_iter_append_basic(&strct, DBUS_TYPE_UINT16, &code) ||
	      !dbus_message_iter_open_container(&strct, DBUS_TYPE_ARRAY, "y", &arr))
	    return 0;
	  for (size_t j = 0; j < len; ++j, ++(*pp))
	    if (!dbus_message_iter_append_basic(&arr, DBUS_TYPE_BYTE, *pp))
	      return 0;
	  if (!dbus_message_iter_close_container(&strct, &arr) ||
	      !dbus_message_iter_close_container(&options, &strct))
	    return 0;

	  rdlen -= 2*sizeof(uint16_t) + len;
	}

      if (!dbus_message_iter_close_container(&pseudoheader, &options) ||
          !dbus_message_iter_close_container(&pseudoheaders, &pseudoheader))
	return 0;
    }

  if (!dbus_message_iter_close_container(args, &pseudoheaders))
    return 0;

  return 1;
}

void emit_dbus_domain_signal(union mysockaddr *source_addr,
                             struct dns_header *header, size_t qlen)
{
  DBusConnection *connection = (DBusConnection *)daemon->dbus;
  DBusMessage* message = NULL;
  DBusMessageIter args;
  dbus_bool_t qr, aa, tc, rd, ra, ad, cd;
  char opcode, rcode;

  unsigned char *p = (unsigned char *)(header+1);

  if (!connection || !header || !qlen || OPCODE(header) != QUERY)
    return;

  // New dbus message
  if (!(message = dbus_message_new_signal(DNSMASQ_PATH, daemon->dbus_name, "ResolveDomain")))
    return;

  // Append arguments
  dbus_message_iter_init_append(message, &args);

  // Append source address
#ifdef HAVE_IPV6
  if (source_addr->sa.sa_family == AF_INET6)
    inet_ntop(AF_INET6, (struct all_addr *)&source_addr->in6.sin6_addr,
        daemon->addrbuff, ADDRSTRLEN);
  else // Suppose this is an IPv4
#endif
    inet_ntop(AF_INET, (struct all_addr *)&source_addr->in.sin_addr,
        daemon->addrbuff, ADDRSTRLEN);

  if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &daemon->addrbuff))
    goto cleanup;

  // Append DNS headers
  qr = header->hb3 & HB3_QR;
  opcode = OPCODE(header);
  aa= header->hb3 & HB3_AA;
  tc= header->hb3 & HB3_TC;
  rd= header->hb3 & HB3_RD;
  ra= header->hb4 & HB4_RA;
  ad= header->hb4 & HB4_AD;
  cd= header->hb4 & HB4_CD;
  rcode = RCODE(header);

  if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT16, &header->id) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &qr) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BYTE, &opcode) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &aa) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &tc) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &rd) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &ra) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &ad) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BOOLEAN, &cd) ||
      !dbus_message_iter_append_basic(&args, DBUS_TYPE_BYTE, &rcode))
    goto cleanup;

  // Append DNS queries
  if (!dbus_message_append_queries(&args, header, qlen, ntohs(header->qdcount), &p))
   goto cleanup;

  // Append DNS answers
  if (!dbus_message_append_answers(&args, header, qlen, ntohs(header->ancount), &p))
   goto cleanup;

  // Append DNS authorities
  if (!dbus_message_append_answers(&args, header, qlen, ntohs(header->nscount), &p))
   goto cleanup;

  // Append DNS additionals
  unsigned char *p1 = p;
  if (!dbus_message_append_answers(&args, header, qlen, ntohs(header->arcount), &p))
   goto cleanup;

  // Append DNS pseudoheaders
  p = p1;
  if (!dbus_message_append_pseudoheaders(&args, header, qlen, ntohs(header->arcount), &p))
   goto cleanup;

  dbus_connection_send(connection, message, NULL);

cleanup:
  dbus_message_unref(message);
}

#endif
